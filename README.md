#################################################
HOW TO BUILD ABCJ-API jar
#################################################
 - Execute "gradle clean jar" command from abcj-api project root directory

#################################################
HOW TO INSTALL FSCLI
#################################################
 - In the root directory of the project there is a file called "fscli", just copy that file in /usr/local/bin.
 - Export a Environment variable called FSCLI_HOME which should point to the build/libs of abcj-api project
 
 #################################################
 HOW TO INSTALL BUILD AND GENERAGE CODE COVERAGE
 #################################################
 ./gradlew compileJava test jacocoTestReport
 ./gradlew sonarqube -Dsonar.projectKey=harshSE_abcj-api -Dsonar.organization=harshse-bitbucket -Dsonar.host.url=https://sonarcloud.io -Dsonar.login=f1eb8186b2e778d30131cd23203368b04f28d035
 bash <(curl -s https://codecov.io/bash)
 java -jar lib/codacy-coverage-reporter-4.0.3-assembly.jar report -l Java -r build/reports/jacoco/test/jacocoTestReport.xml
 https://app.codeship.com/projects/311697#
 
