package com.theroots.jspgenie.elements;

import org.junit.jupiter.api.Test;

public class ElementTest {

    @Test
    public void createDoesNotCallToElementWhenElementsIsNull() {
        Element.create(null, null);
    }
}
