package com.theroots.jspgenie;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;

import com.google.gson.Gson;
import com.theroots.jspgenie.data.JspData;
import com.theroots.jspgenie.data.SettingsData;
import com.theroots.jspgenie.factory.ControllerGenerator;
import com.theroots.jspgenie.factory.JSPFactory;
import com.theroots.jspgenie.factory.IndexJspGenerator;
import com.theroots.jspgenie.factory.PropertiesGenerator;


public class FullStackCLI extends BaseCLI {

	private static final String INSUFFICIENT_ARGUMENTS = "Insufficient arguments";
	private static final String FSCLI = "fscli > ";
	private static final String VIEW = "view";
	private static final String CREATE = "create";
	private SettingsData settings;

	public static void main(String[] args) throws Exception {
		FullStackCLI cli = new FullStackCLI();
		cli.readSettings();
		cli.start(args);
	}

	private void readSettings() throws Exception {
		try {
			settings = new Gson().fromJson(new FileReader("./settings.json"), SettingsData.class);
		} catch (FileNotFoundException ex) {
			System.out.println("Current directory is not project root. Either change directory to project root, or make"
					+ "sure you have settings.json file.");
		}
	}

	@Override
	protected String executeCommand(String command, String parameters) throws Exception {
		if (CREATE.equals(command)) {
			return executeCreateCommand(parameters.split(" "));
		} /*else if (VIEW.equals(command)) {
			return executeViewCommand(parameters.split(" "));
		} */else {
			return executeHelpCommand();
		}
	}

	private String executeHelpCommand() {
		StringBuilder builder = new StringBuilder();
		builder.append("List of commands").append(System.lineSeparator());
		builder.append("create <json> ").append("\n")
			.append("\tGenerates create page from input JSON file").append("\n");
		builder.append("view <json>").append("\n")
			.append("\tGenerates view page from input JSON file").append("\n");
		
		return builder.toString();
	}
	
	@Override
	protected String getConsolePrompt() {
		return FSCLI;
	}

/*	private String executeViewCommand(String args[]) throws Exception {
		StringBuilder builder = new StringBuilder();
		if (args.length != 1) {
			builder.append(INSUFFICIENT_ARGUMENTS).append(System.lineSeparator());
			return builder.toString();
		}

		return new String(Files.readAllBytes(Paths.get(args[0])));
	}*/

	private String executeCreateCommand(String args[])  throws Exception {
		StringBuilder builder = new StringBuilder();
		if (args.length != 1) {
			builder.append(INSUFFICIENT_ARGUMENTS).append(System.lineSeparator());
			return builder.toString();
		}
		
		JspData jspData = new Gson().fromJson(new FileReader(args[0]), JspData.class);
		// {viewDir}/WEB-INF/content/{namespace}/{name}
		String destinationFolder = String.format("%s/WEB-INF/content/%s/%s", settings.getViewDir(), 
				settings.getNamespace(), jspData.getName());
		
		File file = new File(destinationFolder);
		file.mkdirs();
		
		builder.append(generateCreateJsp(jspData, destinationFolder) + "\n");
		builder.append(generateUpdateJsp(jspData, destinationFolder) + "\n");
		builder.append(generateViewJsp(jspData, destinationFolder) + "\n");
		builder.append(generateIndexJsp(jspData, destinationFolder) + "\n");
		builder.append(generateController(jspData) + "\n");
		builder.append(generateProperties(jspData) + "\n");
		return builder.toString();
	}

	private String generateIndexJsp(JspData jspData, String destinationFolder) throws IOException {
		String jspName = jspData.getName() + "-index.jsp";
		String destinationFile = destinationFolder + File.separator + jspName;
		IndexJspGenerator.generate(settings, destinationFile, jspData);
		return jspName + " created successfully";
	}

	private String generateUpdateJsp(JspData jspData, String destinationFolder) throws IOException {
		String jspName = jspData.getName() + "-edit.jsp";
		String destinationFile = destinationFolder + File.separator + jspName;
		createFile(destinationFile, JSPFactory.edit(jspData, settings));
		return jspName + " created successfully";
	}

	private String generateProperties(JspData jspData) throws IOException {
		String controllerDirectory = controllerDir(jspData);
		File controllerDir = new File(controllerDirectory);
		controllerDir.mkdirs();
		String className = initCap(jspData.getName()) + "CTRL_en.properties";
		String propertiesFile = controllerDirectory + className;
		if (isFileExist(propertiesFile) == false) {
			PropertiesGenerator.generate(settings, propertiesFile, jspData);
		}
		return className + " created successfully";
	}

	private boolean isFileExist(String propertiesFile) {
		File file = new File(propertiesFile);
		return file.exists();
	}

	private String controllerDir(JspData jspData) {
		String controllerDirectory = String.format("%s/%s/%s/%s/controller/%s/", 
				settings.getSrcDir(), settings.getGroupId().replaceAll("\\.", File.separator), 
				settings.getProjectId(), settings.getNamespace(), jspData.getName());
		return controllerDirectory;
	}

	private String generateViewJsp(JspData jspData, String destinationFolder) throws IOException {
		String jspName = jspData.getName() + "-show.jsp";
		String destinationFile = destinationFolder + File.separator + jspName;
		createFile(destinationFile, JSPFactory.view(jspData, settings));
		return jspName + " created successfully";
	}

	// {srcDir}/{groupId}/{projectId}/{namespace}/controller/{name}/{name}CTRL.java
	private String generateController(JspData jspData) throws IOException {
		String controllerDirectory = controllerDir(jspData);
		File controllerDir = new File(controllerDirectory);
		controllerDir.mkdirs();

		String className = initCap(jspData.getName()) + "CTRL.java";
		String controllerFile = controllerDirectory + className;
		if (isFileExist(controllerFile)  == false) {
			ControllerGenerator.generate(settings, controllerFile, jspData.getName());
		}
		
		return className + " created successfully";
	}

	private String initCap(String name) {
		return name.substring(0, 1).toUpperCase() + name.substring(1);
	}

	private String generateCreateJsp(JspData jspData, String destinationFolder) throws IOException {
		String jspName = jspData.getName() + "-editNew.jsp";
		String destinationFile = destinationFolder + File.separator + jspName;
		createFile(destinationFile, JSPFactory.create(jspData, settings));

		return jspName + " created successfully";
	}

	private void createFile(String destinationFile, String content) throws IOException {
		Files.write(Paths.get(destinationFile), content.getBytes());
	}
}


