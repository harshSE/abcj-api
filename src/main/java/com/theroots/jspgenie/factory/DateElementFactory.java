package com.theroots.jspgenie.factory;

import com.theroots.jspgenie.data.ElementData;
import com.theroots.jspgenie.data.SettingsData;
import com.theroots.jspgenie.elements.DateElement;
import com.theroots.jspgenie.elements.Element;

public class DateElementFactory implements ElementFactory{
    @Override
    public Element create(ElementData data, SettingsData settings) {
        return new DateElement(data.getName(), data.getKey());
    }
}
