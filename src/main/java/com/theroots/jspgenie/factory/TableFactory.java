package com.theroots.jspgenie.factory;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import com.theroots.jspgenie.data.ElementData;
import com.theroots.jspgenie.data.SettingsData;
import com.theroots.jspgenie.elements.Element;
import com.theroots.jspgenie.elements.ElementType;
import com.theroots.jspgenie.elements.Table;


import static com.theroots.jspgenie.factory.Elements.createElements;

public class TableFactory implements ElementFactory {

    private String moduleName;

    public TableFactory(String moduleName) {
        this.moduleName = moduleName;
    }

    @Override
    public Element create(ElementData data, SettingsData setting) {

        return new Table(data.getName(), data.getTitle(), createElements(data.getElementDatas(), setting, moduleName), data.getModelSource());
    }
}
