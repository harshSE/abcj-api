package com.theroots.jspgenie.factory;

import java.io.IOException;
import java.io.StringWriter;
import java.nio.file.Files;
import java.nio.file.Paths;

import com.theroots.jspgenie.data.SettingsData;
import com.theroots.jspgenie.util.IndentingPrintWriter;

public class ControllerGenerator {

	private IndentingPrintWriter out;
	private SettingsData settings;
	private String controllerFile;
	private String name;
	private StringWriter writer;

	public ControllerGenerator(SettingsData settings, String controllerFile, String name) {
		this.settings = settings;
		this.controllerFile = controllerFile;
		this.name = name;
		writer = new StringWriter();
		out = new IndentingPrintWriter(writer);
	}
	
	public static void generate(SettingsData settings, String controllerFile, String name) throws IOException {
		ControllerGenerator generator = new ControllerGenerator(settings, controllerFile, name);
		generator.generatePackage()
			.generateImports()
			.generateAnnotations()
			.generateClassName()
			.openingBraces()
			.generateClassContent()
			.closingBraces();
		
		Files.write(Paths.get(controllerFile), generator.writer.toString().getBytes());
	}

	private ControllerGenerator generateClassContent() {
		String nameWithInitCap = name.substring(0, 1).toUpperCase() + name.substring(1);
		out.println();
		out.println("@Override");
		out.println(String.format("public %sData createModel() {", nameWithInitCap));
		out.incrementIndentation();
		out.println(String.format("return new %sData();", nameWithInitCap));
		out.decrementIndentation();
		out.println("}");
		out.println();
		out.println("@SuppressWarnings(\"unchecked\")");
		out.println("@SkipValidation");
		out.println("public HttpHeaders index() { ");
		out.incrementIndentation();
		out.println("return new DefaultHttpHeaders(SUCCESS).disableCaching();");
		out.decrementIndentation();
		out.println("}");
		return this;
	}

	private ControllerGenerator generateImports() {
		out.println("import org.apache.struts2.convention.annotation.Namespace;\n" + 
				"import org.apache.struts2.convention.annotation.ParentPackage;\n" + 
				"import org.apache.struts2.convention.annotation.Result;\n" + 
				"import org.apache.struts2.convention.annotation.Results;\n" + 
				"import org.apache.struts2.interceptor.validation.SkipValidation;\n" + 
				"import org.apache.struts2.rest.DefaultHttpHeaders;\n" + 
				"import org.apache.struts2.rest.HttpHeaders;\n" +
				"import static com.opensymphony.xwork2.Action.SUCCESS;");
		
		out.println(String.format("import %s.%s.%s.controller.RestGenericCTRL;", settings.getGroupId(), settings.getProjectId(),
				settings.getNamespace()));
		
		out.println();
		out.println();
		return this;
	}

	private ControllerGenerator closingBraces() {
		out.decrementIndentation();
		out.println("}");
		return this;
	}

	private ControllerGenerator openingBraces() {
		out.println("{");
		out.incrementIndentation();
		return this;
	}

	private ControllerGenerator generateClassName() {
		out.println();
		String nameWithInitCap = name.substring(0, 1).toUpperCase() + name.substring(1);
		out.print(String.format("public class %sCTRL extends RestGenericCTRL<%sData> ", nameWithInitCap, nameWithInitCap));
		return this;
	}

	private ControllerGenerator generateAnnotations() {
		out.println(String.format("@ParentPackage(value = \"%s\")\n" + 
				"@Namespace(\"/%s/%s\")\n" + 
				"@Results({\n" + 
				"@Result(name = SUCCESS, type = \"redirectAction\", params = {\"actionName\", \"%s\"}),\n" + 
				"})", settings.getNamespace(), settings.getNamespace(), name, name));
		return this;
	}

	private ControllerGenerator generatePackage() {
		out.println(String.format("package %s.%s.%s.%s.%s;", settings.getGroupId(), settings.getProjectId(),
				settings.getNamespace(), "controller", name));
		out.println();
		return this;
	}

}
