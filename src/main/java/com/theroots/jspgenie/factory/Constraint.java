package com.theroots.jspgenie.factory;

import com.theroots.jspgenie.util.IndentingPrintWriter;

public class Constraint {
    private int maxLength;
    private String required;

    public Constraint(int maxLength, String required) {
        this.maxLength = maxLength;
        this.required = required;
    }

    public int getMaxLength() {
        return maxLength;
    }

    public String getRequired() {
        return required;
    }

    public void appendTo(IndentingPrintWriter out) {
        if (maxLength > 0) {
            out.append(" maxlength=\"").append(maxLength+"").append("\"");
        }

        if (required != null) {
            out.append(" required=\"").append(required).append("\"");
        }
    }
}
