package com.theroots.jspgenie.factory;

import com.theroots.jspgenie.data.ElementData;
import com.theroots.jspgenie.data.SettingsData;
import com.theroots.jspgenie.elements.Element;
import com.theroots.jspgenie.elements.TextArea;

public class TextAreaFactory implements ElementFactory {
    @Override
    public Element create(ElementData data, SettingsData setting) {
        return new TextArea(data.getName(), data.getKey());
    }
}
