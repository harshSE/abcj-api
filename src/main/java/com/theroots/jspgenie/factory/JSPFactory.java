package com.theroots.jspgenie.factory;

import java.io.StringWriter;
import com.theroots.jspgenie.data.JspData;
import com.theroots.jspgenie.data.SettingsData;
import com.theroots.jspgenie.elements.JspElement;
import com.theroots.jspgenie.util.IndentingPrintWriter;

public class JSPFactory {

    public static String create(JspData data, SettingsData settings) {
        JspElement jspElement = new JspElement(data.getName(),
                Elements.createElements(data.getElementDatas(), settings, data.getName()));
        StringWriter writer = new StringWriter();
        IndentingPrintWriter out = new IndentingPrintWriter(writer);
        jspElement.create(out);
        return writer.toString();
    }

    public static String view(JspData data, SettingsData settings) {
        JspElement jspElement = new JspElement(data.getName(),
                Elements.createElements(data.getElementDatas(), settings, data.getName()));
        StringWriter writer = new StringWriter();
        IndentingPrintWriter out = new IndentingPrintWriter(writer);
        jspElement.view(out);
        return writer.toString();
    }

    public static String edit(JspData data, SettingsData settings) {
        JspElement jspElement = new JspElement(data.getName(),
                Elements.createElements(data.getElementDatas(), settings, data.getName()));
        StringWriter writer = new StringWriter();
        IndentingPrintWriter out = new IndentingPrintWriter(writer);
        jspElement.edit(out);
        return writer.toString();
    }

}
