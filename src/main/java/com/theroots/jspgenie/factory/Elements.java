package com.theroots.jspgenie.factory;

import java.util.ArrayList;
import java.util.List;
import com.theroots.jspgenie.data.ElementData;
import com.theroots.jspgenie.data.SettingsData;
import com.theroots.jspgenie.elements.Element;
import com.theroots.jspgenie.elements.ElementType;

public class Elements {

    static List<Element> createElements(List<ElementData> elementDatas, SettingsData settings, String moduleName) {
        List<Element> elements = new ArrayList<>();

        if (elementDatas == null) {
            return elements;
        }

        for (ElementData data : elementDatas) {

            Element element = createElement(data, settings, moduleName);
            if (element != null) {
                elements.add(element);
            }
        }

        return elements;
    }

    static Element createElement(ElementData data, SettingsData settings, String moduleName) {
        ElementType type = ElementType.fromType(data.getType());
        if (type == null) {
            return null;
        }
        return type.getFactory(settings.getNamespace(), moduleName).create(data, settings);
    }
}
