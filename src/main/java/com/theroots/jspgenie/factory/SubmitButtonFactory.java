package com.theroots.jspgenie.factory;

import com.theroots.jspgenie.data.ElementData;
import com.theroots.jspgenie.data.SettingsData;
import com.theroots.jspgenie.elements.Element;
import com.theroots.jspgenie.elements.SubmitButton;

public class SubmitButtonFactory implements ElementFactory {

    private final String moduleName;
    private String namespace;

    public SubmitButtonFactory(String moduleName, String namespace) {
        this.moduleName = moduleName;
        this.namespace = namespace;
    }

    @Override
    public Element create(ElementData data, SettingsData setting) {
        return new SubmitButton(data.getName(), data.getKey(), namespace, moduleName,  data.getAction());
    }
}
