package com.theroots.jspgenie.factory;

import com.theroots.jspgenie.data.ConstraintsData;
import com.theroots.jspgenie.data.ElementData;
import com.theroots.jspgenie.data.FormData;
import com.theroots.jspgenie.data.SettingsData;
import com.theroots.jspgenie.data.TextFieldData;
import com.theroots.jspgenie.elements.Element;
import com.theroots.jspgenie.elements.TextField;

public class TextFieldFactory implements ElementFactory {
    @Override
    public Element create(ElementData data, SettingsData setting) {
        ConstraintsData constraintsData = data.getConstraintsData();
        Constraint constraint = null;
        if (constraintsData != null) {
            constraint = new Constraint(constraintsData.getMaxLength(), constraintsData.getRequired());
        }
        return new TextField(data.getName(), data.getKey(), constraint);
    }
}
