package com.theroots.jspgenie.factory;

import com.theroots.jspgenie.data.ElementData;
import com.theroots.jspgenie.data.SettingsData;
import com.theroots.jspgenie.elements.Element;
import com.theroots.jspgenie.elements.HorizontalLayoutElement;

public class HorizontalLayoutFactory implements ElementFactory {

    private String moduleName;

    public HorizontalLayoutFactory(String moduleName) {
        this.moduleName = moduleName;
    }

    @Override
    public Element create(ElementData data, SettingsData setting) {
        return new HorizontalLayoutElement(data.getGrid(), Elements.createElements(data.getElementDatas(), setting, moduleName));
    }
}
