package com.theroots.jspgenie.factory;

import com.theroots.jspgenie.data.ElementData;
import com.theroots.jspgenie.data.SettingsData;
import com.theroots.jspgenie.elements.Element;
import com.theroots.jspgenie.elements.Section;

public class SectionFactory implements ElementFactory {

    private String moduleName;

    public SectionFactory(String moduleName) {
        this.moduleName = moduleName;
    }

    @Override
    public Element create(ElementData data, SettingsData setting) {
        return new Section(data.getName(), Elements.createElements(data.getElementDatas(), setting, moduleName));
    }
}
