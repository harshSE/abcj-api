package com.theroots.jspgenie.factory;

import java.io.IOException;
import java.io.StringWriter;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import com.theroots.jspgenie.data.ElementData;
import com.theroots.jspgenie.data.JspData;
import com.theroots.jspgenie.data.SettingsData;
import com.theroots.jspgenie.elements.Element;
import com.theroots.jspgenie.util.IndentingPrintWriter;

public class IndexJspGenerator {

	public static void generate(SettingsData settings, String file, JspData jspData) throws IOException {
		StringWriter writer = new StringWriter();
		IndentingPrintWriter out = new IndentingPrintWriter(writer);
		
		List<ElementData> glanceables = new ArrayList<>();
		List<ElementData> elementDatas = jspData.getElementDatas();
		if (elementDatas != null) {
			elementDatas.forEach(el -> {
				el.addToGlanceables(glanceables);
			});
		}

        out.println("<%@taglib uri=\"/struts-tags/ec\" prefix=\"s\" %>");

        out.incrementIndentation();
        out.println("<div class=\"row\">");
        out.incrementIndentation();
        out.println("<div class=\"col-xs-12\">");
        createPrimaryPanel(out, glanceables, jspData.getName());
        out.println("</div>");
        out.decrementIndentation();
        out.println("</div>");
        out.decrementIndentation();

		Files.write(Paths.get(file), writer.toString().getBytes());
	}

    private static void createPrimaryPanel(IndentingPrintWriter out, List<ElementData> glanceables, String name) {
        out.incrementIndentation();
        out.println("<div class=\"panel panel-primary\">");
        createHeaderPanel(out, name);
        out.incrementIndentation();
        out.println("<div class=\"panel-body\">");
        out.incrementIndentation();
        createTable(out, glanceables);
        out.decrementIndentation();
        out.println("</div>");
        out.decrementIndentation();
        out.println("</div>");
        out.decrementIndentation();

    }

    private static void createHeaderPanel(IndentingPrintWriter out, String name) {
        out.incrementIndentation();
        out.println("<div class=\"panel-heading\">");
        out.incrementIndentation();
        out.println(String.format("<h3 class=\"panel-title\">%s</h3>", name));
        out.decrementIndentation();
        out.println("</div>");
        out.decrementIndentation();
    }

    private static void createTable(IndentingPrintWriter out, List<ElementData> glanceables) {
        out.println("<table class=\"table table-blue\">");
        viewTableHeader(out, glanceables);
        viewTableBody(out, glanceables);
        out.println("</table>");
    }

    private static void viewTableHeader(IndentingPrintWriter out, List<ElementData> glanceables) {
        out.println("<thead>");
        out.incrementIndentation();
        out.println("<tr>");
        out.incrementIndentation();
        for (ElementData glanceable : glanceables) {
            out.println("<th>");
            out.incrementIndentation();
            out.println(String.format("<s:text name=\"%s\"/>", glanceable.getKey()));
            out.decrementIndentation();
            out.println("</th>");
        }
        out.decrementIndentation();
        out.println("<tr>");
        out.decrementIndentation();
        out.println("</thead>");
    }
	
	private static void viewTableBody(IndentingPrintWriter out, List<ElementData> glanceables) {
        out.println("<tbody>");
        out.incrementIndentation();
        viewTableContent(out, glanceables);
        out.decrementIndentation();
        out.println("</tbody>");
    }

    private static void viewTableContent(IndentingPrintWriter out, List<ElementData> glanceables) {
        out.println("<s:iterator value=\"list\">");
        out.println("<tr>");
        out.incrementIndentation();
        if (glanceables != null) {
            for (ElementData element : glanceables) {
                out.println("<td>");
                out.incrementIndentation();
                out.print("<s:property ");
                out.append(" value=\"%{").append(element.getName()).append("}\"");
                out.append(" />");
                out.println();
                out.decrementIndentation();
                out.println("</td>");
            }
        }
        out.decrementIndentation();
        out.println("</tr>");
        out.println("</s:iterator>");
    }
}
