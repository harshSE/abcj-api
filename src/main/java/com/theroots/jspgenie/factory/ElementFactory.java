package com.theroots.jspgenie.factory;

import java.util.ArrayList;
import java.util.List;
import com.theroots.jspgenie.data.SettingsData;
import com.theroots.jspgenie.elements.Element;
import com.theroots.jspgenie.data.ElementData;
import com.theroots.jspgenie.elements.ElementType;

public interface ElementFactory {
    Element create(ElementData data, SettingsData settings);
}
