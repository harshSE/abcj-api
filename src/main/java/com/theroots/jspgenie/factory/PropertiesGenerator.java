package com.theroots.jspgenie.factory;

import java.io.IOException;
import java.io.StringWriter;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;

import com.theroots.jspgenie.data.ElementData;
import com.theroots.jspgenie.data.JspData;
import com.theroots.jspgenie.data.SettingsData;
import com.theroots.jspgenie.util.IndentingPrintWriter;

public class PropertiesGenerator {
	public static void generate(SettingsData settings, String file, JspData jspData) throws IOException {
		StringWriter writer = new StringWriter();
		IndentingPrintWriter out = new IndentingPrintWriter(writer);
		
		addProperties(jspData.getElementDatas(), out);
		
		Files.write(Paths.get(file), writer.toString().getBytes());
	}

	private static void addProperties(List<ElementData> elementDatas, IndentingPrintWriter out) {
		if (elementDatas == null) {
			return;
		}
		
		for (ElementData data : elementDatas) {
			data.appendProperties(out);
		}
	}
}
