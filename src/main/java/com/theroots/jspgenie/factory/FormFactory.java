package com.theroots.jspgenie.factory;

import java.util.ArrayList;
import java.util.List;
import com.theroots.jspgenie.data.SettingsData;
import com.theroots.jspgenie.elements.Element;
import com.theroots.jspgenie.data.AttributeData;
import com.theroots.jspgenie.data.ElementData;
import com.theroots.jspgenie.data.FormData;
import com.theroots.jspgenie.elements.Form;

public class FormFactory implements ElementFactory {

    public static final String ACTION="action";
    public static final String METHOD="method";
    private String moduleName;

    public FormFactory(String moduleName) {
        this.moduleName = moduleName;
    }

    @Override
    public Element create(ElementData data, SettingsData setting) {
        return new Form(moduleName, data.getAction(), setting.getNamespace(), Elements.createElements(data.getElementDatas(), setting, moduleName));
    }
}
