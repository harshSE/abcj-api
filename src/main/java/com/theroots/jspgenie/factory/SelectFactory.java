package com.theroots.jspgenie.factory;

import com.theroots.jspgenie.data.ElementData;
import com.theroots.jspgenie.data.SettingsData;
import com.theroots.jspgenie.elements.Element;
import com.theroots.jspgenie.elements.Select;

public class SelectFactory implements ElementFactory {
    @Override
    public Element create(ElementData data, SettingsData setting) {
        return new Select(data.getName(), data.getModelSource(), data.getModelKey(), data.getModelValue(), data.getJsonSource(), data.getKey(), data.getValue());
    }
}
