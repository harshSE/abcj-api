package com.theroots.jspgenie;


import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map.Entry;
import java.util.Set;

import com.google.gson.Gson;
import com.google.gson.JsonElement;

import jline.console.ConsoleReader;
import jline.console.completer.ArgumentCompleter;
import jline.console.completer.Completer;
import jline.console.completer.NullCompleter;
import jline.console.completer.StringsCompleter;

public abstract class BaseCLI {	
	private static final String MODULE = "BASE-CLI";
	private ConsoleReader consoleReader;
	
	public BaseCLI() {		
	}
	
	public void start(String[] cliArgs) {
		
		//Get CLI Args
		String host = null;
		int port = -1;
		List<String> cliCommandList  = null;
		//executeCommands
		executeCommands(cliCommandList);
	}

	private void executeCommands(List<String> cliCommandList) {
		executeTerminalCommands();
	}

	private void executeTerminalCommands() {
		//Initializing Prompt for CLI
		initTerminal();
		startTerminal();
	}

	private void initTerminal() {
		try {
			consoleReader = new ConsoleReader();
//			consoleReader.clearScreen();
			
			String commandNames = "{'create':{}, 'view':{}}";
			List<Completer> completorList = sorting(buildCompletor(commandNames));
			for(Completer completor : completorList){
				consoleReader.addCompleter(completor);
			}
			String[] hotkeyHelp = new String[] {"'create':{'-help':{}}", "'view':{'-help':{}}"};
			List<Completer[]> completorArray = new ArrayList<Completer[]>();
			for(int i = 0 ; i < hotkeyHelp.length ; i++){
				
				completorArray.addAll(buildCompletor(hotkeyHelp[i]));
			}
			completorList = sorting(completorArray);
			for(Completer completor : completorList){
				consoleReader.addCompleter(completor);
			}
		} catch (Exception e) {
		}finally{
//			Terminal.resetTerminal();
		}
		
	}

	private void startTerminal() {
		displayWelcomeMessage();
		try{
			String promptInput;
			do{			
				promptInput = consoleReader.readLine(getConsolePrompt());
				
				int index = promptInput.indexOf(" ");

				String strCommand = promptInput;
				String strCommadParameters = "";
				if(index != -1){
					strCommand = promptInput.substring(0, index);
					strCommadParameters = promptInput.substring(index + 1, promptInput.length());
				}
				if (!promptInput.equalsIgnoreCase("q")) {
					try {
						displayResult(executeCommand(strCommand, strCommadParameters));
					} catch (Exception ex) {
						ex.printStackTrace();
						displayResult("This was not expected to happen.");
					}
				}
				
			} while(!promptInput.equalsIgnoreCase("q"));
			
		}catch(IOException e){
			System.out.println(e.getMessage());
		}finally{
//			Terminal.resetTerminal();
		}
	}
	
	protected abstract String executeCommand(String command, String parameters) throws IOException, Exception;

	protected void newLine() throws IOException {
		System.out.println();
	}
	
	protected void displayResult(String result) throws IOException {
		System.out.println(result);
		consoleReader.flush();
	}
	
	protected String getConsolePrompt(){
		return "$ ";		
	}
	
	public String getServerDescription(){
		return "";
	}	
	private void displayWelcomeMessage(){		
		System.out.println();		
		System.out.println("************************************************");
		System.out.println("*  Anybody Can Do Full Stack! CLI              *");
		System.out.println("************************************************");
		System.out.println();
		System.out.println("Enter ? or help for list of commands supported.");
		System.out.println();

	}

	public static List<Completer> sorting(List<Completer[]> listCompletor){
		List<Completer> newCompletorList = new ArrayList<Completer>();
		//sorting
		for(int i = 0 ; i < listCompletor.size() ; i++){
			for(int j = 0 ; j < i ; j++){
				if(listCompletor.get(i).length < listCompletor.get(j).length){
					Completer[] completor = listCompletor.get(i);
					listCompletor.set(i, listCompletor.get(j));					
					listCompletor.set(j, completor);
				}
			}
		}
		//arranging
		
		for(Completer[] completor : listCompletor){
			Completer argCompletor = new ArgumentCompleter(completor);
			newCompletorList.add(argCompletor);
		}
		return newCompletorList;
	}
	
	public static List<Completer[]> buildCompletor(String inputString){
		List<Completer[]> listCompletor = new ArrayList<Completer[]>();
		inputString = inputString.replaceAll("\n","\\\\n");
		JsonElement commandJSONObj = new Gson().fromJson(inputString, JsonElement.class);
		recursion(listCompletor, commandJSONObj);
		return listCompletor;
	}

	public static void recursion(List<Completer[]> listCompletor,JsonElement jsonObj,String... parentList){
	
		Set<Entry<String,JsonElement>> entries = jsonObj.getAsJsonObject().entrySet();
		List<String> optionList = new ArrayList<>();
		for(Entry<String,JsonElement> entry  : entries){
			String key = entry.getKey();
			optionList.add(key);
			JsonElement childObj = entry.getValue();
			if(childObj.isJsonObject() && childObj.getAsJsonObject().entrySet().isEmpty() == false){
				String[] newParentList = null;
				if(parentList != null && parentList.length <= 0){
					newParentList = new String[1];
				}else{
					newParentList = new String[parentList.length + 1];
					System.arraycopy(parentList, 0, newParentList, 0, parentList.length);
				}
				newParentList[newParentList.length-1] = key;
				
				recursion(listCompletor, childObj, newParentList);
			}				
		}
		if(optionList.size() > 0 ){
			
			Completer[] completorList = new Completer[parentList.length + 2];			
			if(parentList.length > 0){
				for(int i = 0 ; i < parentList.length ; i++ ){
					completorList[i] = new StringsCompleter(parentList[i]);
				}
			}			
			String[] optionArray = new String[optionList.size()];
			optionArray = optionList.toArray(optionArray);			
			completorList[completorList.length-2] = new StringsCompleter(optionArray);
			completorList[completorList.length-1] = new NullCompleter();
			listCompletor.add(completorList);
		}
	}
}
