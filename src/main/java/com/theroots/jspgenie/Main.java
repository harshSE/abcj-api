package com.theroots.jspgenie;

import java.io.FileReader;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import com.google.gson.Gson;
import com.theroots.jspgenie.data.JspData;
import com.theroots.jspgenie.data.SettingsData;
import com.theroots.jspgenie.factory.JSPFactory;

public class Main {

    public static void main(String[] args) throws IOException {
        JspData jspData = new Gson().fromJson(new FileReader("/mnt/data/development/abcj-api/src/main/resources/subscriber.json"), JspData.class);
//        JspData jspData = new Gson().fromJson(inputDataJson, JspData.class);

        System.out.println(jspData);

        String jspContent;
        SettingsData settings = new SettingsData();
        if(args.length >= 1 && args[0].equalsIgnoreCase("view")) {
            jspContent = JSPFactory.view(jspData, settings);
        } else {

            jspContent = JSPFactory.create(jspData, settings);
        }
        System.out.println(jspContent);

        Files.write(Paths.get("/opt/codefest/demo-webapps/src/main/webapp/new.jsp"), jspContent.getBytes());

//        Files.write("/opt/codefest/demo-webapps/src/main/webapp/new.jsp", jspContent.getBytes(), StandardOpenOption.WRITE);
    }
}
