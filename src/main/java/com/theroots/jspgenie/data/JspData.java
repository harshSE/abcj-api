package com.theroots.jspgenie.data;

import java.util.List;
import com.google.gson.annotations.SerializedName;

public class JspData {
    @SerializedName("name")
    private String name;
    @SerializedName("elements")
    private List<ElementData> elementDatas;

    public JspData() {
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<ElementData> getElementDatas() {
        return elementDatas;
    }

    public void setElementDatas(List<ElementData> elementDatas) {
        this.elementDatas = elementDatas;
    }

    @Override
    public String toString() {
        return "JspData{" +
                "name='" + name + '\'' +
                ", elementDatas=" + elementDatas +
                '}';
    }
}
