package com.theroots.jspgenie.data;

import com.google.gson.annotations.SerializedName;

public class TextFieldData {
    @SerializedName("name")
    private String name;
    @SerializedName("key")
    private String key;

    public TextFieldData() {
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return "TextFieldData{" +
                "name='" + name + '\'' +
                ", key='" + key + '\'' +
                '}';
    }
}
