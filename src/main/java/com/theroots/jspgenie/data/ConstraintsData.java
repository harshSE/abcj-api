package com.theroots.jspgenie.data;

import com.google.gson.annotations.SerializedName;

public class ConstraintsData {
    @SerializedName("maxlength")
    private int maxLength;
    private String required;
    private String dataType;

    public ConstraintsData() {
    }

    public int getMaxLength() {
        return maxLength;
    }

    public void setMaxLength(int maxLength) {
        this.maxLength = maxLength;
    }

    public String getRequired() {
        return required;
    }

    public void setRequired(String required) {
        this.required = required;
    }

    public String getDataType() {
        return dataType;
    }

    public void setDataType(String dataType) {
        this.dataType = dataType;
    }

    @Override
    public String toString() {
        return "ConstraintsData{" +
                "maxLength='" + maxLength + '\'' +
                ", required='" + required + '\'' +
                ", dataType='" + dataType + '\'' +
                '}';
    }
}
