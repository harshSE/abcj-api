package com.theroots.jspgenie.data;

import com.google.gson.annotations.SerializedName;

public class FormData {

    @SerializedName("attribute")
    private AttributeData attributeData;
    @SerializedName("textfield")
    private TextFieldData textFieldData;

    public FormData() {

    }

    public AttributeData getAttributeData() {
        return attributeData;
    }

    public void setAttributeData(AttributeData attributeData) {
        this.attributeData = attributeData;
    }

    @Override
    public String toString() {
        return "FormData{" +
                "attributeData=" + attributeData +
                ", textFieldData=" + textFieldData +
                '}';
    }
}
