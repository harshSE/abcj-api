package com.theroots.jspgenie.data;

import com.google.gson.annotations.SerializedName;

public class AttributeData {
    @SerializedName("action")
    private String action;
    @SerializedName("method")
    private String method;

    public AttributeData() {
    }

    public String getAction() {
        return action;
    }

    public void setAction(String action) {
        this.action = action;
    }

    @Override
    public String toString() {
        return "AttributeData{" +
                "action='" + action + '\'' +
                ", method='" + method + '\'' +
                '}';
    }
}
