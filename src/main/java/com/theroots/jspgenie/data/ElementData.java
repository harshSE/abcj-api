package com.theroots.jspgenie.data;

import java.util.Arrays;
import java.util.List;
import com.google.gson.annotations.SerializedName;
import com.theroots.jspgenie.util.IndentingPrintWriter;

public class ElementData {
    private String type;
    private String name;
    private String key;
    private String value;
    private String action;
    @SerializedName("model-source")
    private String modelSource;
    @SerializedName("model-key")
    private String modelKey;
    @SerializedName("model-value")
    private String modelValue;
    @SerializedName("json-source")
    private String jsonSource;
    @SerializedName("constraints")
    private ConstraintsData constraintsData;
    @SerializedName("elements")
    private List<ElementData> elementDatas;
    @SerializedName("title")
    private String[] title;
    private int grid;
    private String glanceable;

    public ElementData() {
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public String getAction() {
        return action;
    }

    public void setAction(String action) {
        this.action = action;
    }

    public ConstraintsData getConstraintsData() {
        return constraintsData;
    }

    public void setConstraintsData(ConstraintsData constraintsData) {
        this.constraintsData = constraintsData;
    }

    public List<ElementData> getElementDatas() {
        return elementDatas;
    }

    public void setElementDatas(List<ElementData> elementDatas) {
        this.elementDatas = elementDatas;
    }

    public String getJsonSource() {
        return jsonSource;
    }

    public String getModelSource() {
        return modelSource;
    }

    public void setModelSource(String modelSource) {
        this.modelSource = modelSource;
    }

    public void setJsonSource(String jsonSource) {
        this.jsonSource = jsonSource;
    }

    public String getModelKey() {
        return modelKey;
    }

    public void setModelKey(String modelKey) {
        this.modelKey = modelKey;
    }

    public String getModelValue() {
        return modelValue;
    }

    public void setModelValue(String modelValue) {
        this.modelValue = modelValue;
    }

    public void setTitle(String[] title) {
        this.title = title;
    }

    public int getGrid() {
        return grid;
    }

    public void setGrid(int grid) {
        this.grid = grid;
    }

    public String[] getTitle() {
        return title;
    }

    @Override
    public String toString() {
        return "ElementData{" +
                "type='" + type + '\'' +
                ", name='" + name + '\'' +
                ", key='" + key + '\'' +
                ", value='" + value + '\'' +
                ", action='" + action + '\'' +
                ", modelSource='" + modelSource + '\'' +
                ", modelKey='" + modelKey + '\'' +
                ", modelValue='" + modelValue + '\'' +
                ", jsonSource='" + jsonSource + '\'' +
                ", constraintsData=" + constraintsData +
                ", elementDatas=" + elementDatas +
                ", title=" + Arrays.toString(title) +
                ", grid='" + grid + '\'' +
                '}';
    }

	public void appendProperties(IndentingPrintWriter out) {
		if (key != null && name != null) {
			out.println(String.format("%s=%s", key, name));
		}
		
		if (elementDatas == null) {
			return;
		}
		
		for (ElementData data : elementDatas) {
			data.appendProperties(out);
		}
	}
	
	public String getGlanceable() {
		return glanceable;
	}

	public void addToGlanceables(List<ElementData> glanceables) {
		if ("true".equalsIgnoreCase(glanceable)) {
			glanceables.add(this);
		}
		
		if (elementDatas != null) {
			elementDatas.forEach(el -> el.addToGlanceables(glanceables));
		}
	}
}