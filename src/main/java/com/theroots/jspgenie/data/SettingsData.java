package com.theroots.jspgenie.data;

public class SettingsData {

	private String groupId;
	private String projectId;
	private String namespace;
	private String srcDir;
	private String viewDir;
	
	
	public String getViewDir() {
		return viewDir;
	}
	public void setViewDir(String viewDir) {
		this.viewDir = viewDir;
	}
	public String getSrcDir() {
		return srcDir;
	}
	public void setSrcDir(String srcDir) {
		this.srcDir = srcDir;
	}
	public String getGroupId() {
		return groupId;
	}
	public void setGroupId(String groupId) {
		this.groupId = groupId;
	}
	public String getProjectId() {
		return projectId;
	}
	public void setProjectId(String projectId) {
		this.projectId = projectId;
	}
	public String getNamespace() {
		return namespace;
	}
	public void setNamespace(String namespace) {
		this.namespace = namespace;
	}

	
}
