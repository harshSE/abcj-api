package com.theroots.jspgenie.elements;

import java.util.Objects;
import java.util.UUID;
import com.theroots.jspgenie.factory.ElementFactory;
import com.theroots.jspgenie.util.IndentingPrintWriter;

public class SubmitButton implements Element{

    private final String name;
    private final String key;
    private final String namespace;
    private final String moduleName;
    private String action;

    public SubmitButton(String name, String key, String namespace, String moduleName, String action) {

        this.name = name;
        this.key = key;
        this.namespace = namespace;
        this.moduleName = moduleName;
        this.action = action;
    }

    @Override
    public String name() {
        return name;
    }

    @Override
    public String key() {
        return key;
    }

    @Override
    public void create(IndentingPrintWriter out) {
        out.println("<div class=\"row\">");
        out.incrementIndentation();
        out.println("<div class=\"col-xs-12\" align=\"center\">");
        out.incrementIndentation();
        out.print("<button class=\"btn btn-primary btn-sm\" type=\"submit\" role=\"submit\" ");
        
        out.append(String.format("id=\"submit_%s\">", UUID.randomUUID().toString()));
        out.println();

        out.incrementIndentation();
        out.println(String.format("<span class=\"glyphicon glyphicon-floppy-disk\"></span> <s:text name=\"%s\"/>", name()));
        out.decrementIndentation();
        out.println("</button>");
        out.decrementIndentation();
        out.println("</div>");
        out.decrementIndentation();
        out.println("</div>");
    }

    @Override
    public void view(IndentingPrintWriter out) {
    }

	@Override
	public void edit(IndentingPrintWriter out) {
		out.println("<div class=\"row\">");
        out.incrementIndentation();
        out.println("<div class=\"col-xs-12\" align=\"center\">");
        out.incrementIndentation();
        out.print("<button class=\"btn btn-primary btn-sm\" type=\"submit\" role=\"submit\" ");
        out.append(String.format("formaction=\"${pageContext.request.contextPath}/%s/%s/%s/${id}\" ", namespace, moduleName, action));

        out.append(String.format("id=\"submit_%s\">", UUID.randomUUID().toString()));
        out.println();

        out.incrementIndentation();
        out.println(String.format("<span class=\"glyphicon glyphicon-floppy-disk\"></span> <s:text name=\"%s\"/>", name()));
        out.decrementIndentation();
        out.println("</button>");
        out.decrementIndentation();
        out.println("</div>");
        out.decrementIndentation();
        out.println("</div>");		
	}
}
