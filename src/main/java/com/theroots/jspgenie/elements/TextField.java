package com.theroots.jspgenie.elements;

import org.apache.commons.lang3.StringUtils;

import com.theroots.jspgenie.factory.Constraint;
import com.theroots.jspgenie.util.IndentingPrintWriter;

public class TextField implements Element {
    private String name;
    private String key;
    private Constraint constraint;

    public TextField(String name, String key, Constraint constraint) {
        this.name = name;
        this.key = key;
        this.constraint = constraint;
    }

    public String getName() {
        return name;
    }

    public String getKey() {
        return key;
    }

    public Constraint getConstraint() {
        return constraint;
    }

    @Override
    public String name() {
        return name;
    }

    @Override
    public String key() {
        return key;
    }

    @Override
    public void create(IndentingPrintWriter out) {

        out.print("<s:textfield ");
        if(StringUtils.isBlank(key) == false) {
            append(out, "key", key);
        }


        append(out,"name", name);

        if (constraint != null) {
            constraint.appendTo(out);
        }

        out.append(" cssClass=\"form-control\" />");
        out.println();
    }

    @Override
    public void createTable(String modelSource, IndentingPrintWriter out) {
        out.print("<s:textfield ");
        if(StringUtils.isBlank(key) == false) {
            append(out, "key", key);
        }


        append(out,"name", String.format("%s[0].%s", modelSource, name));

        if (constraint != null) {
            constraint.appendTo(out);
        }

        out.append(" cssClass=\"form-control\" />");
        out.println();
    }

    private void append(IndentingPrintWriter out, String lable, String value) {
        out.append(" ").append(lable).append("=\"").append(value).append("\"");
    }

    @Override
    public void view(IndentingPrintWriter out) {
        out.print("<s:label ");
        if(StringUtils.isBlank(key) == false) {
            append(out, "key", key);
        }

        out.append(" value=\"%{").append(name).append("}\"");

        out.append(" cssClass=\"control-label light-text\" labelCssClass=\"col-xs-5\" elementCssClass=\"col-xs-7\"/>");
        out.println();
    }
    
    @Override
    public void edit(IndentingPrintWriter out) {
    	create(out);
    }
    
    @Override
    public void tableView(IndentingPrintWriter out) {
        out.print("<s:property ");

        out.append(" value=\"%{").append(name).append("}\"");

        out.append("/>");

        out.println();
    }
}
