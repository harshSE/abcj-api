package com.theroots.jspgenie.elements;

import java.util.List;
import java.util.Objects;
import com.theroots.jspgenie.util.IndentingPrintWriter;

public class Form implements Element {
    private String moduleName;
    private String action;
    private String namespace;
    private List<Element> childElements;

    public Form(String moduleName, String action, String namespace, List<Element> childElements) {
        this.moduleName = moduleName;
        this.action = action;
        this.namespace = namespace;
        this.childElements = childElements;
    }

    public String getAction() {
        return action;
    }

    public List<Element> getChildElements() {
        return childElements;
    }

    @Override
    public String name() {
        return null;
    }

    @Override
    public String key() {
        return null;
    }

    @Override
    public void create(IndentingPrintWriter out) {
        out.print("<s:form cssClass=\"form-horizontal\" validate=\"true\" labelCssClass=\"col-xs-12 col-sm-4 col-lg-3\" elementCssClass=\"col-xs-12 col-sm-8 col-lg-9\" onsubmit=\"return validateForm()\" ");

        if(Objects.isNull(namespace)) {
            out.append("namespace=\"/\"");
        } else {
            out.append(String.format("namespace=\"/%s/%s\"", namespace, moduleName));
        }


        if (action == null) {
            out.append(">");
            out.println();
        } else {
            out.append(" ").append("action").append("=")
                    .append("\"")
                    .append(action)
                    .append("\">");
            out.println();
        }
        out.incrementIndentation();
        out.println("<div class=\"row\">");
        out.incrementIndentation();
        out.println("<div class=\"col-xs-12\">");
        out.incrementIndentation();
        Element.create(childElements, out);
        out.decrementIndentation();
        out.println("</div>");
        out.decrementIndentation();
        out.println("</div>");
        out.decrementIndentation();
        out.println("</s:form>");
    }

    @Override
    public void view(IndentingPrintWriter out) {
        Element.view(childElements, out);
    }

	@Override
	public void edit(IndentingPrintWriter out) {
		out.print("<s:form cssClass=\"form-horizontal\" validate=\"true\" labelCssClass=\"col-xs-12 col-sm-4 col-lg-3\" elementCssClass=\"col-xs-12 col-sm-8 col-lg-9\" onsubmit=\"return validateForm()\" ");

        if(Objects.isNull(namespace)) {
            out.append("namespace=\"/\"");
        } else {
            out.append(String.format("namespace=\"/%s/%s\"", namespace, moduleName));
        }

        if (action == null) {
            out.append(">");
            out.println();
        } else {
            out.append(" ").append("action").append("=")
                    .append("\"")
                    .append(action)
                    .append("\">");
            out.println();
        }

        out.println("<s:hidden name=\"_method\" value=\"put\" />");
        out.incrementIndentation();
        out.println("<div class=\"row\">");
        out.incrementIndentation();
        out.println("<div class=\"col-xs-12\">");
        out.incrementIndentation();
        Element.edit(childElements, out);
        out.decrementIndentation();
        out.println("</div>");
        out.decrementIndentation();
        out.println("</div>");
        out.decrementIndentation();
        out.println("</s:form>");
	}
}
