package com.theroots.jspgenie.elements;

import com.theroots.jspgenie.util.IndentingPrintWriter;

public class Select implements Element {
    private String name;
    private String modelSource;
    private String modelKey;
    private String modelValue;
    private String jsonSource;
    private String key;
    private String value;

    public Select(String name, String modelSource, String modelKey, String modelValue, String jsonSource, String key, String value) {
        this.name = name;
        this.modelSource = modelSource;
        this.modelKey = modelKey;
        this.modelValue = modelValue;
        this.jsonSource = jsonSource;
        this.key = key;
        this.value = value;
    }

    public String getModelSource() {
        return modelSource;
    }

    public String getJsonSource() {
        return jsonSource;
    }

    public String getKey() {
        return key;
    }

    public String getValue() {
        return value;
    }

    public String getModelKey() {
        return modelKey;
    }

    public String getModelValue() {
        return modelValue;
    }

    @Override
    public String name() {
        return name;
    }

    @Override
    public String key() {
        return key;
    }

    @Override
    public void create(IndentingPrintWriter out) {
        out.print("<s:select");

        out.append(String.format(" name=\"%s\"", name));
        if (key != null) {
            out.append(String.format(" key=\"%s\"", key));
        }

        if (modelSource != null) {
            out.append(" list=\"").append(modelSource).append("\"");
        }

        if (modelKey != null) {
            out.append(" listKey=\"").append(modelKey).append("\"");
        }

        if (modelValue != null) {
            out.append(" listValue=\"").append(modelValue).append("\"");;
        }

        if (jsonSource != null) {
            out.append(" list=\"#").append(jsonSource).append("\"");
        }

        out.append(" cssClass=\"form-control\" />");
        out.println();
    }

    @Override
    public void createTable(String modelSource, IndentingPrintWriter out) {
        out.print("<s:select");

        out.append(String.format(" name=\"%s[0].%s\"", modelSource, name));
        if (key != null) {
            out.append(String.format(" key=\"%s\"", key));
        }

        if (modelSource != null) {
            out.append(" list=\"").append(this.modelSource).append("\"");
        }

        if (modelKey != null) {
            out.append(" listKey=\"").append(modelKey).append("\"");
        }

        if (modelValue != null) {
            out.append(" listValue=\"").append(modelValue).append("\"");
        }

        if (jsonSource != null) {
            out.append(" list=\"#").append(jsonSource).append("\"");
        }

        out.append(" cssClass=\"form-control\" />");
        out.println();
    }

    @Override
    public void view(IndentingPrintWriter out) {
        new TextField(name, key, null).view(out);
    }

	@Override
	public void edit(IndentingPrintWriter out) {
		create(out);
	}
    @Override
    public void tableView(IndentingPrintWriter out) {
        new TextField(name, key, null).tableView(out);
    }
}
