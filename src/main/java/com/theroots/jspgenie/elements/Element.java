package com.theroots.jspgenie.elements;

import java.util.List;
import java.util.Objects;
import com.theroots.jspgenie.util.IndentingPrintWriter;

public interface Element {
    String name();
    String key();

    void create(IndentingPrintWriter out);
    void view(IndentingPrintWriter out);
    void edit(IndentingPrintWriter out);

    default void createTable(String modelSource, IndentingPrintWriter out) {

    }

    default void tableView(IndentingPrintWriter out) {
        out.print("<s:property ");

        out.append(" value=\"%{").append(name()).append("}\"");

        out.append(" />");
        out.println();
    }

    static void create(List<Element> elements, IndentingPrintWriter out) {
        if(Objects.nonNull(elements)) {
            elements.forEach(element -> element.create(out));
        }
    }

    static void view(List<Element> elements, IndentingPrintWriter out) {
        if(Objects.nonNull(elements)) {
            elements.forEach(element -> element.view(out));
        }

    }
	static void edit(List<Element> elements, IndentingPrintWriter out) {
		if(Objects.nonNull(elements)) {
            elements.forEach(element -> element.edit(out));
        }
	}
}
