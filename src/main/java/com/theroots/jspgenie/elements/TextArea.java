package com.theroots.jspgenie.elements;

import com.theroots.jspgenie.util.IndentingPrintWriter;
import org.apache.commons.lang3.StringUtils;

public class TextArea implements Element{

    private String name;
    private String key;

    public TextArea(String name, String key) {
        this.name = name;
        this.key = key;
    }

    public String getName() {
        return name;
    }

    public String getKey() {
        return key;
    }

    @Override
    public String name() {
        return name;
    }

    @Override
    public String key() {
        return key;
    }

    @Override
    public void create(IndentingPrintWriter out) {
        out.print("<s:textarea ");
        if(StringUtils.isBlank(key) == false) {
            append(out, "key", name);
        }

        append(out,"name", name);

        out.append(" cssClass=\"form-control\" />");
        out.println();
    }

    private void append(IndentingPrintWriter out, String lable, String value) {
        out.append(" ").append(lable).append("=\"").append(value).append("\"");
    }

    @Override
    public void view(IndentingPrintWriter out) {
        new TextField(name, key, null).view(out);
    }

	@Override
	public void edit(IndentingPrintWriter out) {
		create(out);
	}
	
    @Override
    public void tableView(IndentingPrintWriter out) {
        new TextField(name, key, null).tableView(out);
    }
}
