package com.theroots.jspgenie.elements;

import java.util.List;
import com.theroots.jspgenie.util.IndentingPrintWriter;

public class HorizontalLayoutElement implements Element {

    private int grid;
    private List<Element> childElements;

    public HorizontalLayoutElement(int grid, List<Element> elements) {
        this.grid = grid;
        this.childElements = elements;
    }

    @Override
    public String name() {
        return null;
    }

    @Override
    public String key() {
        return null;
    }

    public int getGrid() {
        return grid;
    }

    @Override
    public void create(IndentingPrintWriter out) {
        out.println("<div class=\"row\">");
        out.incrementIndentation();
        out.println(String.format("<div class=\"col-xs-%s\">", grid));
        out.incrementIndentation();
        Element.create(childElements, out);
        out.decrementIndentation();
        out.println("</div>");
        out.decrementIndentation();
        out.println("</div>");
    }

    @Override
    public void view(IndentingPrintWriter out) {
        out.println("<div class=\"row\">");
        out.incrementIndentation();
        out.println(String.format("<div class=\"col-xs-%s\">", grid));
        out.incrementIndentation();
        Element.view(childElements, out);
        out.decrementIndentation();
        out.println("</div>");
        out.decrementIndentation();
        out.println("</div>");
    }

	@Override
	public void edit(IndentingPrintWriter out) {
		create(out);
	}
}
