package com.theroots.jspgenie.elements;

import java.util.List;
import com.theroots.jspgenie.util.IndentingPrintWriter;

public class Section implements Element {

    private String name;
    private List<Element> elements;

    public Section(String name, List<Element> elements) {
        this.name = name;
        this.elements = elements;
    }

    public String getName() {
        return name;
    }

    public List<Element> getElements() {
        return elements;
    }

    @Override
    public String name() {
        return null;
    }

    @Override
    public String key() {
        return null;
    }

    @Override
    public void create(IndentingPrintWriter out) {
        out.println("<fieldset class=\"fieldSet-line\">");
        out.incrementIndentation();
        out.println("<legend align=\"top\">");
        out.incrementIndentation();
        out.print("<s:text name=\"");
        out.append(name).append("\">").append("</s:text>");
        out.println();
        out.decrementIndentation();
        out.println("</legend>");
        out.decrementIndentation();
        Element.create(elements, out);
        out.println("</fieldset>");
    }

    @Override
    public void view(IndentingPrintWriter out) {
        out.println("<fieldset class=\"fieldSet-line\">");
        out.incrementIndentation();
        out.println("<legend align=\"top\">");
        out.incrementIndentation();
        out.print("<s:text name=\"");
        out.append(name).append("\">").append("</s:text>");
        out.println();
        out.decrementIndentation();
        out.println("</legend>");
        out.decrementIndentation();
        Element.view(elements, out);
        out.println("</fieldset>");
    }

	@Override
	public void edit(IndentingPrintWriter out) {
		create(out);
	}
}
