package com.theroots.jspgenie.elements;

import java.util.HashMap;
import java.util.Map;
import com.theroots.jspgenie.factory.DateElementFactory;
import com.theroots.jspgenie.factory.ElementFactory;
import com.theroots.jspgenie.factory.FormFactory;
import com.theroots.jspgenie.factory.HorizontalLayoutFactory;
import com.theroots.jspgenie.factory.SectionFactory;
import com.theroots.jspgenie.factory.SelectFactory;
import com.theroots.jspgenie.factory.SubmitButtonFactory;
import com.theroots.jspgenie.factory.TableFactory;
import com.theroots.jspgenie.factory.TextAreaFactory;
import com.theroots.jspgenie.factory.TextFieldFactory;
import com.theroots.jspgenie.factory.VerticalLayoutFactory;

public enum ElementType {
    FORM {
        @Override
        public ElementFactory getFactory(String namespace, String moduleName) {
            return new FormFactory(moduleName);
        }
    },
    TEXTFIELD {
        @Override
        public ElementFactory getFactory(String namespace, String moduleName) {
            return new TextFieldFactory();
        }
    },
    TEXTAREA{
        @Override
        public ElementFactory getFactory(String namespace, String moduleName) {
            return new TextAreaFactory();
        }
    },
    SELECT {
        @Override
        public ElementFactory getFactory(String namespace, String moduleName) {
            return new SelectFactory();
        }
    },
    SECTION {
        @Override
        public ElementFactory getFactory(String namespace, String moduleName) {
            return new SectionFactory(moduleName);
        }
    },
    TABLE {
        @Override
        public ElementFactory getFactory(String namespace, String moduleName) {
            return new TableFactory(moduleName);
        }
    },
    SUBMIT {
        @Override
        public ElementFactory getFactory(String namespace, String moduleName) {
            return new SubmitButtonFactory(moduleName, namespace);
        }
    },
    HORIZONTAL_LAYOUT {
        @Override
        public ElementFactory getFactory(String namespace, String moduleName) {
            return new HorizontalLayoutFactory(moduleName);
        }
    },
    VERTICAL_LAYOUT {
        @Override
        public ElementFactory getFactory(String namespace, String moduleName) {
            return new VerticalLayoutFactory(moduleName);
        }
    },
    DATE {
        @Override
        public ElementFactory getFactory(String namespace, String moduleName) {
            return new DateElementFactory();
        }
    }
    ;

    private static Map<String, ElementType> map = new HashMap<>();

    static {
        for (ElementType type : ElementType.values()) {
            map.put(type.name().toLowerCase(), type);
        }
    }

    public abstract ElementFactory getFactory(String namespace, String moduleName);

    public static ElementType fromType(String type) {
        return map.get(type.toLowerCase());
    }
}
