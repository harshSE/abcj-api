package com.theroots.jspgenie.elements;

import com.theroots.jspgenie.util.IndentingPrintWriter;
import org.apache.commons.lang3.StringUtils;

public class DateElement implements Element {

    private String name;
    private String key;

    public DateElement(String name, String key) {
        this.name = name;
        this.key = key;
    }

    public String getName() {
        return name;
    }

    public String getKey() {
        return key;
    }

    @Override
    public String name() {
        return name;
    }

    @Override
    public String key() {
        return key;
    }

    @Override
    public void create(IndentingPrintWriter out) {

        out.print("<s:textfield ");
        if(StringUtils.isBlank(key) == false) {
            append(out, "key", key);
        }
        append(out,"name", name);

        out.append(" type=\"date\" cssClass=\"form-control\" />");
        out.println();
    }

    @Override
    public void createTable(String modelSource, IndentingPrintWriter out) {
        out.print("<s:textfield ");
        if(StringUtils.isBlank(key) == false) {
            append(out, "key", key);
        }
        append(out,"name", String.format("%s[0].%s", modelSource, name));

        out.append(" type=\"date\" cssClass=\"form-control\" />");
        out.println();
    }

    private void append(IndentingPrintWriter out, String lable, String value) {
        out.append(" ").append(lable).append("=\"").append(value).append("\"");
    }

    @Override
    public void view(IndentingPrintWriter out) {
        out.print("<s:label ");
        if(StringUtils.isBlank(key) == false) {
            append(out, "key", key);
        }

        out.append(" value=\"%{").append(name).append("}\"");

        out.append(" cssClass=\"control-label light-text\" labelCssClass=\"col-xs-5\" elementCssClass=\"col-xs-7\"/>");
        out.println();
    }

	@Override
	public void edit(IndentingPrintWriter out) {
		create(out);
	}
}
