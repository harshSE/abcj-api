package com.theroots.jspgenie.elements;

import java.io.StringWriter;
import java.util.List;
import com.theroots.jspgenie.util.IndentingPrintWriter;

public class JspElement implements Element {

    private String name;
    private List<Element> elements;

    public JspElement(String name, List<Element> elements) {
        this.name = name;
        this.elements = elements;
    }

    public String getName() {
        return name;
    }

    public List<Element> getElements() {
        return elements;
    }

    @Override
    public String name() {
        return name;
    }

    @Override
    public String key() {
        return null;
    }

    @Override
    public void create(IndentingPrintWriter out) {
        
        out.println("<%@taglib uri=\"/struts-tags/ec\" prefix=\"s\" %>");

        out.incrementIndentation();
        out.println("<div class=\"row\">");
        out.incrementIndentation();
        out.println("<div class=\"col-xs-12\">");
        createPrimaryPanel(out);
        out.println("</div>");
        out.decrementIndentation();
        out.println("</div>");
        out.decrementIndentation();
    }

    private void editPrimaryPanel(IndentingPrintWriter out) {
        out.incrementIndentation();
        out.println("<div class=\"panel panel-primary\">");
        createHeaderPanel(out);
        out.incrementIndentation();
        out.println("<div class=\"panel-body\">");
        out.incrementIndentation();
        Element.edit(elements, out);
        out.decrementIndentation();
        out.println("</div>");
        out.decrementIndentation();
        out.println("</div>");
        out.decrementIndentation();
    }
    
    private void createPrimaryPanel(IndentingPrintWriter out) {
        out.incrementIndentation();
        out.println("<div class=\"panel panel-primary\">");
        createHeaderPanel(out);
        out.incrementIndentation();
        out.println("<div class=\"panel-body\">");
        out.incrementIndentation();
        Element.create(elements, out);
        out.decrementIndentation();
        out.println("</div>");
        out.decrementIndentation();
        out.println("</div>");
        out.decrementIndentation();
    }

    private void createHeaderPanel(IndentingPrintWriter out) {
        out.incrementIndentation();
        out.println("<div class=\"panel-heading\">");
        out.incrementIndentation();
        out.println(String.format("<h3 class=\"panel-title\">%s</h3>", name()));
        out.decrementIndentation();
        out.println("</div>");
        out.decrementIndentation();
    }

    @Override
    public void view(IndentingPrintWriter out) {
        out.println("<%@taglib uri=\"/struts-tags/ec\" prefix=\"s\" %>");
        out.incrementIndentation();
        out.println("<div class=\"row\">");
        out.incrementIndentation();
        out.println("<div class=\"col-xs-12\">");
        viewPrimaryPanel(out);
        out.println("</div>");
        out.decrementIndentation();
        out.println("</div>");
        out.decrementIndentation();
    }

    private void viewPrimaryPanel(IndentingPrintWriter out) {
        out.incrementIndentation();
        out.println("<div class=\"panel panel-primary\">");
        viewEditAndDeleteButton(out);
        out.incrementIndentation();
        out.println("<div class=\"panel-body\">");
        out.incrementIndentation();
        Element.view(elements, out);
        out.decrementIndentation();
        out.println("</div>");
        out.decrementIndentation();
        out.println("</div>");
        out.decrementIndentation();
    }

    private void viewEditAndDeleteButton(IndentingPrintWriter out) {
        out.incrementIndentation();
        out.println("<div class=\"panel-heading\" style=\"padding: 8px 15px\">");
        out.incrementIndentation();
        out.println("<h3 class=\"panel-title\">");
        out.println(String.format("<s:text name=\"%s\"/>", name()));
        out.println("</h3>");
        out.decrementIndentation();
        out.incrementIndentation();
        out.println("<div class=\"nv-btn-group\" align=\"right\">");
        out.incrementIndentation();
        out.println("<span class=\"btn-group btn-group-xs\">");
        out.incrementIndentation();
        out.println("<button type=\"button\" class=\"btn btn-default header-btn\" data-toggle=\"tooltip\" data-placement=\"bottom\" title=\"<s:text name=\"audit.history\"/>/");
        out.append(String.format(" onclick=\"javascript:location.href='${pageContext.request.contextPath}/sm/audit/audit/${id}?auditableResourceName=${name}&refererUrl=/sm/%s/%s/${id}'\">",name(),name()));
        out.incrementIndentation();
        out.println("<span class=\"glyphicon glyphicon-eye-open\" ></span>");
        out.decrementIndentation();
        out.println("</button>");
        out.decrementIndentation();
        out.println("</span>");
        out.decrementIndentation();
        out.println("<span class=\"btn-group btn-group-xs\">");
        out.incrementIndentation();
        out.println("<button type=\"button\" class=\"btn btn-default header-btn\" data-toggle=\"tooltip\" data-placement=\"bottom\" title=\"edit\"");
        out.println(String.format(" onclick=\"javascript:location.href='${pageContext.request.contextPath}/sm/%s/%s/${id}/edit'\">",name(),name()));
        out.incrementIndentation();
        out.println("<span class=\"glyphicon glyphicon-pencil\"></span>");
        out.decrementIndentation();
        out.println("</button>");
        out.decrementIndentation();
        out.println("</span>");
        out.incrementIndentation();
        out.println(String.format(" <span class=\"btn-group btn-group-xs\" data-toggle=\"confirmation-singleton\" onmousedown=\"deleteConfirm()\" data-href=\"${pageContext.request.contextPath}/sm/%s/%s/${id}?_method=DELETE\">",name(),name()));
        out.incrementIndentation();
        out.println("<button type=\"button\" class=\"btn btn-default header-btn\" data-toggle=\"tooltip\" data-placement=\"bottom\" title=\"delete\">");
        out.incrementIndentation();
        out.println("<span class=\"glyphicon glyphicon-trash\"></span>");
        out.decrementIndentation();
        out.println("</button>");
        out.decrementIndentation();
        out.println("</span>");
        out.decrementIndentation();
        out.println("</div>");
        out.decrementIndentation();
        out.println("</div>");
        out.decrementIndentation();
    }

    @Override
    public String toString() {
        StringWriter writer = new StringWriter();
        IndentingPrintWriter out = new IndentingPrintWriter(writer);
        create(out);
        return writer.toString();
    }

	public void edit(IndentingPrintWriter out) {
        out.println("<%@taglib uri=\"/struts-tags/ec\" prefix=\"s\" %>");
        out.incrementIndentation();
        out.println("<div class=\"row\">");
        out.incrementIndentation();
        out.println("<div class=\"col-xs-12\">");
        editPrimaryPanel(out);
        out.println("</div>");
        out.decrementIndentation();
        out.println("</div>");
        out.decrementIndentation();
	}
}
