package com.theroots.jspgenie.elements;

import java.util.List;
import java.util.Objects;
import com.theroots.jspgenie.util.IndentingPrintWriter;
import org.apache.commons.lang3.StringUtils;

public class Table implements Element {

    private String name;
    private String[] titles;
    private List<Element> elements;
    private String modelSource;

    public Table(String name, String[] titles, List<Element> elements, String modelSource) {
        this.name = name;
        this.titles = titles;
        this.elements = elements;
        this.modelSource = modelSource;
    }

    public List<Element> getElements() {
        return elements;
    }

    @Override
    public String name() {
        return name;
    }

    @Override
    public String key() {
        return null;
    }

    @Override
    public void create(IndentingPrintWriter out) {
        out.println("<table class=\"table table-blue\">");
        out.incrementIndentation();
        addCaption(out);
        createTableHeader(out);
        createTableBody(out);
        out.decrementIndentation();
        out.println("</table>");
    }

    private void addCaption(IndentingPrintWriter out) {

        out.println("<caption class=\"caption\">");
        if (StringUtils.isBlank(name) == false) {
            out.incrementIndentation();
            out.println(name);
            out.decrementIndentation();
        }
        out.incrementIndentation();
        out.println("<div align=\"right\" class=\"display-btn\">");
        out.incrementIndentation();
        out.println("<span class=\"btn btn-group btn-group-xs defaultBtn\" onclick=\"add();\" id=\"addRow\">");
        out.incrementIndentation();
        out.println("<span class=\"glyphicon glyphicon-plus\" tabindex=\"10\"></span>");
        out.decrementIndentation();
        out.println("</span>");
        out.decrementIndentation();
        out.println("</div>");
        out.decrementIndentation();
        out.println("</caption>");

    }

    private void createTableBody(IndentingPrintWriter out) {
        out.println("<tbody>");
        out.incrementIndentation();
        createTableContent(out);
        out.decrementIndentation();
        out.println("</tbody>");
    }

    private void createTableContent(IndentingPrintWriter out) {
        out.println("<tr>");
        out.incrementIndentation();
        if (elements != null) {
            for (Element element : elements) {
                out.println("<td>");
                out.incrementIndentation();
                element.createTable(modelSource, out);
                out.decrementIndentation();
                out.println("</td>");
            }
        }
        out.decrementIndentation();
        out.println("</tr>");
    }

    private void createTableHeader(IndentingPrintWriter out) {
        out.println("<thead>");
        out.incrementIndentation();
        out.println("<tr>");
        out.incrementIndentation();
        for (String title : titles) {
            out.println("<th>");
            out.incrementIndentation();
            out.println(String.format("<s:text name=\"%s\"/>", title));
            out.decrementIndentation();
            out.println("</th>");
        }
        out.decrementIndentation();
        out.println("<tr>");
        out.decrementIndentation();
        out.println("</thead>");
    }

    @Override
    public void view(IndentingPrintWriter out) {
        out.println("<table class=\"table table-blue\">");
        out.incrementIndentation();
        viewCaption(out);
        viewTableHeader(out);
        viewTableBody(out);
        out.decrementIndentation();
        out.println("</table>");
    }

    private void viewCaption(IndentingPrintWriter out) {

        out.println("<caption class=\"caption\">");
        if (StringUtils.isBlank(name) == false) {
            out.incrementIndentation();
            out.println(name);
            out.decrementIndentation();
        }
        out.println("</caption>");

    }

    private void viewTableBody(IndentingPrintWriter out) {
        out.println("<tbody>");
        out.incrementIndentation();
        viewTableContent(out);
        out.decrementIndentation();
        out.println("</tbody>");
    }

    private void viewTableContent(IndentingPrintWriter out) {
        if(Objects.nonNull(modelSource)) {
            out.println(String.format("<s:iterator value=\"%s\">", modelSource));
        }

        out.println("<tr>");
        out.incrementIndentation();
        if (elements != null) {
            for (Element element : elements) {
                out.println("<td>");
                out.incrementIndentation();
                element.tableView(out);
                out.decrementIndentation();
                out.println("</td>");
            }
        }
        out.decrementIndentation();
        out.println("</tr>");
        if(Objects.nonNull(modelSource)) {
            out.println("</s:iterator>");
        }
    }

    private void viewTableHeader(IndentingPrintWriter out) {
        out.println("<thead>");
        out.incrementIndentation();
        out.println("<tr>");
        out.incrementIndentation();
        for (String title : titles) {
            out.println("<th>");
            out.incrementIndentation();
            out.println(String.format("<s:text name=\"%s\"/>", title));
            out.decrementIndentation();
            out.println("</th>");
        }
        out.decrementIndentation();
        out.println("<tr>");
        out.decrementIndentation();
        out.println("</thead>");
    }

	@Override
	public void edit(IndentingPrintWriter out) {
		create(out);
	}

}
